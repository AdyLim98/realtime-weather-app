import React from 'react'
import { View, Text ,Platform ,StyleSheet} from 'react-native'
import {Picker} from '@react-native-community/picker'

export default function picker({unitsTemperature,setUnitsTemperature}) {
    changeUnit = (val) =>{
        setUnitsTemperature(val)
    }
    return (
        <View>
            <Picker selectedValue={unitsTemperature} onValueChange={(val)=>changeUnit(val)}>
                <Picker.Item label="°C" value="metric"/>
                <Picker.Item label="°F" value="imperial"/>
            </Picker>
        </View>
    )
}
//if both components using the same style ,but some component want to have specific style then can write the code below
// {{...styles.example , fontSize:10 ,color={PRIMARY}}}   | something like left hand side using ... to combine

const styles = StyleSheet.create({
    pickerPosition:{
        position:'absolute',
        //due to there will be different size between different platform and device type
        //so we can write ... Platform below to modify the style
        ...Platform.select({
            ios:{
                top:-20
            },
            android:{
                top:20
            },
        }),
        left:20,
        height:50,
        width:100,
    }
})
