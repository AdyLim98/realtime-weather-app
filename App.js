import { StatusBar } from 'expo-status-bar';
import React, { useEffect , useState  } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
//useEffect,useState is hooks purpose
import WeatherInfo from './components/weatherInfo'; 
import Picker from './components/picker';
import {WEATHER_API_KEY} from 'react-native-dotenv';

//go to https://home.openweathermap.org/api_keys and get the api key
// npm add -D react-native-dotenv and then create a new file called .env for hide store the secret
// const WEATHER_API_KEY = 'e592d791e0f8527242d2e41d7dd4bc24'
const BASE_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/weather'
const colors = {
  PRIMARY:"#ff304f",
  SECONDARY:"#002651",
  BORDER:"#dbdbdb"
}

//gitignore file is to add to ignore the specific file to upload to gitlab or else
export default function App() {
  
  const [errorMessage , setErrorMessage] = useState(null);
  const [currentWeather , setCurrentWeather] = useState(null)
  //can see the weather api doc by that website and see the units params (metric,imperial and others)
  const [unitsTemperature,setUnitsTemperature] = useState('metric')

  //like created in vuejs I think
  useEffect(()=>{
    load()
  },[unitsTemperature])
  //above [] here means no dependencies
  //unitsTemperature change and make the api call again so u can see the Farenheit / Celcius will change when u select the picker

  async function load(){
    try{
      //To get the permission from user about Location Access
      let { status } = await Location.requestPermissionsAsync()

      if(status != "granted"){
        setErrorMessage("Access the location is necessary to run the application")
        // console.log("ErrorMessage",errorMessage)
        return
      }
      //To get your person now location
      const location = await Location.getCurrentPositionAsync()
      // console.log("Location-",location)

      //To get only latitude and longtitude from location object above
      const {latitude,longitude} = location.coords
      
      // const getWeatherAPI = 'api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}'
      const getWeatherAPI = BASE_WEATHER_URL +"?lat="+latitude+"&lon="+longitude+"&units="+unitsTemperature+"&appid="+WEATHER_API_KEY
      
      //use await fetch for api calls
      const response = await fetch(getWeatherAPI)
      const result = await response.json()
      
      if(response.ok){
        setCurrentWeather(result)
      }else{
        setErrorMessage(result.message)
      }
 
    }catch(error){
      setErrorMessage(error.message)
    }
  }
  
  if(currentWeather){
    // const {main:{temp}} = currentWeather;
    return (
      <View style={styles.container}>
        <Picker unitsTemperature={unitsTemperature} setUnitsTemperature={setUnitsTemperature}/>
        <WeatherInfo info={currentWeather}/>
      </View>
    );
  }else{
    return (
      <View style={styles.container}>
        <Text>{errorMessage}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
